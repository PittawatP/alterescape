﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartEffect : MonoBehaviour
{
    public ParticleSystem HeartPS;
    public GameObject Heart;
    [SerializeField]
    AudioSource Ending;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            HeartPS.Play();
            Ending.Play();
            Destroy(this.gameObject,2);
        }
    }
    
}
