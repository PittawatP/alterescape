﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject Player;
    public bool isDead = false;
    public bool isPause = false;
    public bool isComplete = false;
    [SerializeField]
    public static GameManager Insatance;
    public Vector3 Pos;

    [SerializeField]
    private GameObject UIFail;
 
    [SerializeField] Button Home;
    [SerializeField] Button Reset;


    [SerializeField]
    private GameObject UIComplete;
    [SerializeField] Button HomeComplete;
    [SerializeField] Button ResetComplete;

   [SerializeField]
    private GameObject UIPause;
    [SerializeField] Button HomePause;
    [SerializeField] Button ResetPause;
    [SerializeField] Button UnPause;

    [SerializeField] GameObject Pause;
    
    public bool GameStart = false;


    public static GameManager instant { get; private set; }
    public bool MusicEnabled { get; set; } = true;
    public bool SFXEnabled { get; set; } = true;

    // Start is called before the first frame update
    private void Awake()
    {
        if (Insatance == null)
        {
            Insatance = this;
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        instant = this;
        UIFail.SetActive(false);
        UIPause.SetActive(false);
        UIComplete.SetActive(false);
        SetupDelegate();
      
    }
    void SetupDelegate()
    {
        Home.onClick.AddListener(delegate { GoToHome(Home); });
        Reset.onClick.AddListener(delegate { ResetLevel(Reset); });
      HomePause.onClick.AddListener(delegate { GoToHome(HomePause); });
        ResetPause.onClick.AddListener(delegate { ResetLevel(ResetPause); });
        UnPause.onClick.AddListener(delegate { UnPauseButton(UnPause); });
        HomeComplete.onClick.AddListener(delegate { GoToHome(Home); });
        
    }
    public void GoToHome(Button button) 
    { 
        SceneManager.LoadScene("MainMenu");
        isPause = false;
        isDead = false;
        isComplete = false;
        UIFail.SetActive(false);
        UIPause.SetActive(false);
        UIComplete.SetActive(false);
        GameStart = false;
    }
    public void ResetLevel(Button button)
    {
        UIFail.SetActive(false);
        UIComplete.SetActive(false);
        isPause = false;
        Player.transform.position = new Vector2(Pos.x, Pos.y);
        isDead = false;
        isComplete = false;

    }
    public void UnPauseButton(Button button) { isPause = false; }
    // Update is called once per frame
    void Update()
    {   if (Player==null)
        {
            Player = GameObject.FindGameObjectWithTag("Player");
        }
        
        if (!isDead&&Player.transform.position.y<-50)
        {
            isDead = true;

        }
        if (isDead)
        {
            UIFail.SetActive(true);

        }
        
        if(isPause)
        {
            UIPause.SetActive(true);
        }else if(!isPause)
            UIPause.SetActive(false);

        if(isComplete)
        {
            UIComplete.SetActive(true);
        }if(!isComplete)
        {
            UIComplete.SetActive(false);
        }
        if(GameStart)
        {
            Pause.SetActive(true);
        }
        else
        {
            Pause.SetActive(false);
        }
    }
    public void PauseButton()
    {
        isPause = true;
    }
}
