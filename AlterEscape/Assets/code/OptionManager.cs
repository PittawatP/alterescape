﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionManager : MonoBehaviour
{

    [SerializeField] Toggle _toggleMusic;
    [SerializeField] Toggle _toggleSFX;

    [SerializeField] Button _backButton;
    private void Start()
    {

        _toggleMusic.isOn = GameManager.instant.MusicEnabled;
        _toggleSFX.isOn = GameManager.instant.SFXEnabled;

        _toggleMusic.onValueChanged.AddListener(delegate { OnToggleMusic(_toggleMusic); });
        _toggleSFX.onValueChanged.AddListener(delegate { OnToggleSFX(_toggleSFX); });
        _backButton.onClick.AddListener(delegate { BackToMainMenuButtonClick(_backButton); });
        //SceneMainMenu
    }


    public void OnToggleMusic(Toggle toggle)
    {
        GameManager.instant.MusicEnabled = _toggleMusic.isOn;
        if (GameManager.instant.MusicEnabled)
            SoundManager.Instance.MusicVolume = SoundManager.Instance.MusicVolumeDefault;
        else
            SoundManager.Instance.MusicVolume = SoundManager.MUTE_VOLUME;
    }
    public void OnToggleSFX(Toggle toggle)
    {
        GameManager.instant.SFXEnabled = _toggleSFX.isOn;
        if (GameManager.instant.SFXEnabled)
            SoundManager.Instance.MasterSFXVolume = SoundManager.Instance.MasterSFXVolumeDefault;
        else
            SoundManager.Instance.MasterSFXVolume = SoundManager.MUTE_VOLUME;

    }
    public void BackToMainMenuButtonClick(Button button)
    {

        SceneManager.UnloadSceneAsync("Option");
    }
}
