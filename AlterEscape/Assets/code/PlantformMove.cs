﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantformMove : MonoBehaviour
{
    [SerializeField]
    

   
    private Vector3 pos;

    [SerializeField]
    private float speed;

    [SerializeField]
    private Rigidbody2D rb;
    public bool MoveX=true;
    public bool MoveY;

    [SerializeField]
    private float Range=0;
    private void Start()
    {
        
        rb = GetComponent<Rigidbody2D>();
        pos = transform.position;
        if (MoveX)
            rb.velocity = Vector2.right;
        if (MoveY)
            rb.velocity = Vector2.up;

    }

    private void Update()
    {
         transform.Translate(rb.velocity *(int) (Time.deltaTime*speed));
      
       if(MoveX)
        {
           
            if (rb.position.x>pos.x + Range || rb.position.x < pos.x-Range)
            {
                rb.velocity = -rb.velocity; ;

            }
            
        }
        if (MoveY)
        {
         
            if (rb.position.y > pos.y + Range || rb.position.y < pos.y - Range)
            {
                rb.velocity = -rb.velocity; ;

            }
        }
      


    }
    private void FixedUpdate()
    {
        







    }

}
