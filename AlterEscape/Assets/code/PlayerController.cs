﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(BoxCollider2D))]
public class PlayerController : MonoBehaviour
{
    [SerializeField, Tooltip("Max speed, in units per second, that the character moves.")]
    float speed = 9;


    [SerializeField, Tooltip("Acceleration while in the air.")]
    float airAccelerations = 30;

    [SerializeField]
    ParticleSystem DashDust;
    [SerializeField]
    ParticleSystem JumpDust;
    [SerializeField]
    ParticleSystem DeadPS;

    [SerializeField]
    public float SlideSpeed = 2;

    [SerializeField]
    private float DashSpeed = 1;


    private Vector2 velocity;
    [SerializeField]
    private bool grounded;
    private bool isspikeDead = false;

    private int Direction = 0;

    private Rigidbody2D rb;

    [SerializeField]
    private BoxCollider2D L_Wall;

    [SerializeField]
    private BoxCollider2D R_Wall;



    [SerializeField]
    private BoxCollider2D GroundCheck;

    [SerializeField]
    private LayerMask GroundLayer;




    private bool IsSlid = false;

    [SerializeField]
    private bool Dash = false;
    private bool candash = false;

    public Joystick joy;

    [SerializeField, Tooltip("Max height the character will jump regardless of gravity")]
    float jumpHeight = 4;
    private bool canJump = true;

    [SerializeField]
    Vector2 WallJumpDirection = new Vector2(0, 2);
    [SerializeField]
    private bool IsWall;
    [SerializeField]
    private LayerMask WallLayer;
    [SerializeField]
    private bool wallJump = false;
    private int Wall_LR = 1;

    [SerializeField]
    private float fallMultiplier = 2.5f;
    [SerializeField]
    private float lowMultiplier = 2f;
    [SerializeField]
    private float startdelay = 1f;
    private float DashTime;

    private float moveInputX;
    private float moveInputY;
    private Animator animtor;
    private SpriteRenderer SpriteRen;


    private AudioSource walk;
    [SerializeField]
    private AudioSource JumpAS;
    [SerializeField]
    private AudioClip JumpAClip;
    [SerializeField]
    private AudioSource DashAS;
    [SerializeField]
    private AudioClip DashAClip;
    [SerializeField]
    private AudioSource DeadAS;
    [SerializeField]
    private AudioClip DeadAClip;
    [SerializeField]
    private bool isWalk;
    private bool DashPress;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animtor = GetComponent<Animator>();
        SpriteRen = GetComponent<SpriteRenderer>();
        DashTime = startdelay;
        walk = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (!GameManager.Insatance.isDead && !GameManager.Insatance.isPause) {
            animtor.SetBool("IsDie", false);
            if (moveInputX < 0)
            {
                SpriteRen.flipX = true;
            }
            else
                SpriteRen.flipX = false;

            transform.Translate(velocity * Time.deltaTime);
            if (joy.Horizontal >= .2f || Input.GetAxis("Horizontal") >= .2f)
            {
                moveInputX = speed;
                isWalk = true;

            }
            else if (joy.Horizontal <= -.2f || Input.GetAxis("Horizontal") <= -.2f)
            {
                moveInputX = -speed;
                isWalk = true;

            }
            else
            {
                moveInputX = 0;
                isWalk = false;
            }
            if (joy.Vertical >= .2f || Input.GetAxis("Vertical") >= .2f)
            {
                moveInputY = speed;
            }
            else if (joy.Vertical <= -.2f || Input.GetAxis("Vertical") <= -.2f)
            {
                moveInputY = -speed;
            }
            else moveInputY = 0;


            Vector2 dir = new Vector2(moveInputX, moveInputY);
            if (moveInputX > 0)
            {
                Direction = 1;

            }

            else if (moveInputX < 0)
            {
                Direction = -1;

            }


            Walk(dir);
            PauseMode();
            Colli();
            betterjump();

            WallJump();
            DashOn(dir);
            WallSlide();
            animtor.SetInteger("VelocityY", (int)rb.velocity.y);
            if (Input.GetButtonDown("Jump"))
            {
                isWalk = false;

                if (grounded && canJump)
                {

                    Jump(Vector2.up);
                    animtor.SetBool("IsJump", true);

                }

                if (!grounded && IsWall)
                {
                    wallJump = true;
                }

            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                Dash = true;
                isWalk = false;
                DashPress = true;
            }
        }
        //if(GameManager.Insatance.isDead)
        //{
        //    animtor.SetBool("IsDie", true);

        //}
        if (isspikeDead == true)
        {
            animtor.SetBool("IsDie", true);
        }
        if (isWalk == true)
        {
            if (!walk.isPlaying)
                walk.Play();
        }
        else
        {
            walk.Stop();
        }


    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Trap"))
        {

            isspikeDead = true;
            DeadPS.Play();
            DeadAS.PlayOneShot(DeadAClip);
            isWalk = false;
            Invoke("DeadUI", 1);

        }
        if (collision.gameObject.CompareTag("End"))
        {
            Invoke("Complete", 3);
        }

    }

    private void DeadUI()
    {
        GameManager.Insatance.isDead = true;
        isspikeDead = false;
        DeadPS.Stop();
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(L_Wall.transform.position, L_Wall.size);

        Gizmos.color = Color.red;
        Gizmos.DrawCube(R_Wall.transform.position, R_Wall.size);

        Gizmos.color = Color.blue;
        Gizmos.DrawCube(GroundCheck.transform.position, GroundCheck.transform.localScale);
    }

    void DashOn(Vector3 dir)
    {

        if (DashPress == true && candash == true)
        {
            DashAS.PlayOneShot(DashAClip);
            DashPress = false;
        }
        if (Dash && candash)
        {


            if (DashTime <= 0)
            {

                DashTime = startdelay;
                DashDust.Stop();
                candash = false;
                Dash = false;
                rb.velocity = Vector2.zero;
            }
            else
            {
                DashTime -= Time.deltaTime;

                DashDust.Play();
                //rigth
                if (dir.x >= 1 && dir.y == 0)
                {
                    rb.velocity = (new Vector2(Vector2.right.x * DashSpeed, 0));


                }//left
                if (dir.x <= -1 && dir.y == 0)
                {
                    rb.velocity = (new Vector2(Vector2.left.x * DashSpeed, 0));

                }
                if (dir.y >= 1 && dir.x == 0)
                {
                    rb.velocity = (new Vector2(0, Vector2.up.y * DashSpeed));


                }
                if (dir.y <= -1 && dir.x == 0)
                {
                    rb.velocity = (new Vector2(0, Vector2.down.y * DashSpeed));


                }
                if (dir.x >= 1 && dir.y >= 1)
                {
                    rb.velocity = (new Vector2(Vector2.right.x * DashSpeed, Vector2.up.y * DashSpeed));


                }
                if (dir.x >= 1 && dir.y <= -1)
                {
                    rb.velocity = (new Vector2(Vector2.right.x * DashSpeed, Vector2.down.y * DashSpeed));


                }
                if (dir.x <= -1 && dir.y >= 1)
                {
                    rb.velocity = (new Vector2(Vector2.left.x * DashSpeed, Vector2.up.y * DashSpeed));

                }
                if (dir.x <= -1 && dir.y <= -1)
                {
                    rb.velocity = (new Vector2(Vector2.left.x * DashSpeed, Vector2.down.y * DashSpeed));

                }


            }



        }


    }

    private void Walk(Vector2 dir)
    {
        if (grounded)
        {
            rb.velocity = (new Vector2(dir.x, rb.velocity.y));
            animtor.SetInteger("IsRun", (int)moveInputX);

        }

        else if (!grounded && !IsWall && dir.x != 0)
        { if (rb.velocity.y > 0)
                rb.AddForce(new Vector2(airAccelerations * dir.x, 0));
            if (Mathf.Abs(rb.velocity.x) > speed)
            {
                rb.velocity = new Vector2(dir.x, rb.velocity.y);
            }
            isWalk = false;
            //   rb.velocity = Vector2.Lerp(rb.velocity, (new Vector2(dir.x * speed, rb.velocity.y)), .5f * Time.deltaTime);
        }


    }
    private void Jump(Vector2 dir)
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * jumpHeight;
        canJump = false;
        JumpDust.Play();
        JumpAS.PlayOneShot(JumpAClip);


    }
    private void WallSlide()
    {
        if (IsWall && !grounded && rb.velocity.y < 0)
        {
            IsSlid = true;
        }
        else
            IsSlid = false;

        if (IsSlid)
        {
            rb.velocity = new Vector2(rb.velocity.x, SlideSpeed);
        }

    }
    private void Colli()
    {
        grounded = (BoxCollider2D)Physics2D.OverlapBox(GroundCheck.transform.position, GroundCheck.transform.localScale, 0, GroundLayer);
        bool left = (BoxCollider2D)Physics2D.OverlapBox(L_Wall.transform.position, L_Wall.size, 0, WallLayer);
        bool rigth = (BoxCollider2D)Physics2D.OverlapBox(R_Wall.transform.position, R_Wall.size, 0, WallLayer);
        IsWall = left || rigth;



        if (left)
        {
            Wall_LR = 1;


        }

        if (rigth)
        {
            Wall_LR = -1;



        }
        if (grounded)
        {
            wallJump = false;
            candash = true;
            canJump = true;
            if (rb.velocity.y == 0)
            {
                animtor.SetBool("IsJump", false);
                JumpDust.Stop();
            }
        }



    }
    private void WallJump()
    {
        if ((IsSlid || IsWall) && wallJump)
        {
            rb.velocity = (new Vector2(jumpHeight * Wall_LR * WallJumpDirection.x, jumpHeight * WallJumpDirection.y));
            wallJump = false;
            JumpAS.PlayOneShot(JumpAClip);
            JumpDust.Play();
        }

        // rb.velocity = new Vector2(WallJumpDirection.x * Wall_LR*speed, WallJumpDirection.y*jumpHeight);
        // Invoke("SetJumpingToFalse", 0.05f);


    }

    private void betterjump()
    {
        //if (grounded)
        //{
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;

        }
        else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowMultiplier - 1) * Time.deltaTime;

        }
        // }
    }

    public void jumpbutton()
    {
        if (grounded && canJump)
        {
            Jump(Vector2.up);
            animtor.SetBool("IsJump", true);
        }

        if (!grounded && IsWall)
        {
            wallJump = true;
        }
    }
    public void DashButton()
    {
        Dash = true;
        isWalk = false;
        DashPress = true;



    }
    public void PauseMode()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.Insatance.isPause = true;
        }
        
    }
    
    public void Complete()
    {
        GameManager.Insatance.isComplete = true;
    }
}
