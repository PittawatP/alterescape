﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantformSingle : MonoBehaviour
{
    [SerializeField]
    private BoxCollider2D CheckOn;

    [SerializeField]
    private bool IsOn = false;

    [SerializeField]
    private LayerMask Layer;

    [SerializeField]
    private BoxCollider2D box;


    // Start is called before the first frame update
    void Start()
    {
        box = GetComponent<BoxCollider2D>();
     

    }

    // Update is called once per frame
    void Update()
    {
        
       IsOn = (BoxCollider2D)Physics2D.OverlapBox(CheckOn.transform.position,new Vector2 (transform.localScale.x,CheckOn.size.y), 0, Layer);


       
        if (IsOn)
        {
            box.isTrigger = false;
      
            gameObject.layer = 8;
        }
        else
        {
            box.isTrigger = true;
          
            gameObject.layer = 0;

        }
       
            
    }
}
