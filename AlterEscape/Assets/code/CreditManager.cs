﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class CreditManager : MonoBehaviour
{
    [SerializeField] Button _backButton;
    public void Start()
    {
        _backButton.onClick.AddListener(delegate { BackToMainMenuButtonClick(_backButton); });
    }
    public void BackToMainMenuButtonClick(Button button)
    {

        SceneManager.LoadScene("MainMenu");
    }
}
