﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour, IPointerEnterHandler
{


    [SerializeField] Button _startButton;
    [SerializeField] Button _optionsButton;

    [SerializeField] Button _exitButton;


    AudioSource audiosourceButtomUI;
    [SerializeField] AudioClip audioclipHoldOver;



    private void Start()
    {
       this.audiosourceButtomUI = this.gameObject.GetComponent<AudioSource>();
     //this.audiosourceButtomUI.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("MasterSFXVolume")[0];
        SetupDelegate();

    //   if (!SoundManager.Instance.BGMSource.isPlaying)
    //       SoundManager.Instance.BGMSource.Play();


    }
    void SetupDelegate()
    {
        _startButton.onClick.AddListener(delegate { 
            StartButtonClick(_startButton);
            
        });
        _optionsButton.onClick.AddListener(delegate { OptionsButtonClick(_optionsButton); });
      
        _exitButton.onClick.AddListener(delegate { ExitButtonCilck(_exitButton); });

      //  _SoundTestButton.onClick.AddListener(delegate { SoundTestButtonClick(_SoundTestButton); });
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        //if (audiosourceButtomUI.isPlaying)
        //    audiosourceButtomUI.Stop();

        audiosourceButtomUI.PlayOneShot(audioclipHoldOver);
        
    }
    public void StartButtonClick(Button button) 
    { 
        SceneManager.LoadScene("Level 1");
        GameManager.instant.GameStart = true;
        
    }

    public void OptionsButtonClick(Button button)
    {
      // if (!GameApplicationManager.Instance.IsOptionMenuActive)
      // {
         SceneManager.LoadScene("Option",LoadSceneMode.Additive);
        
        //     GameApplicationManager.Instance.IsOptionMenuActive = true;
        // }

    }
  
    
   
    public void ExitButtonCilck(Button button)
    {
        SceneManager.LoadScene("Credit");
        
    }


}



